import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:networking_learn/detail_screen.dart';

import 'add_screen.dart';
import 'album_model.dart';

Future<List<AlbumModel>> fetchAlbum() async {
  final response =
  await http.get(Uri.parse('https://jsonplaceholder.typicode.com/albums'));

  if (response.statusCode == 200) {
    return compute(parseAlbums, response.body);
  } else {
    throw Exception('Failed to load album');
  }
}

List<AlbumModel> parseAlbums(String responseBody) {
  List<dynamic> body = jsonDecode(responseBody);

  return body.map((album) => AlbumModel.fromJson(album)).toList();
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late Future<List<AlbumModel>> _futureAlbum;

  @override
  void initState() {
    super.initState();
    _futureAlbum = fetchAlbum();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Home Screen"),
      ),
      body: FutureBuilder(
        future: _futureAlbum,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<AlbumModel> albums = snapshot.data as List<AlbumModel>;
            return ListView.builder(
              itemCount: albums.length,
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text(albums[index].title!),
                  onTap: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) =>
                            DetailScreen(albumId: albums[index].id!)));
                  },
                );
              },
            );
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add),
          onPressed: () {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => const AddScreen()));
          }),
    );
  }
}
