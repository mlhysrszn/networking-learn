import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:networking_learn/album_model.dart';

Future<AlbumModel> fetchAlbum(int id) async {
  final response = await http
      .get(Uri.parse('https://jsonplaceholder.typicode.com/albums/$id'));

  if (response.statusCode == HttpStatus.ok) {
    return AlbumModel.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to load album');
  }
}

Future<AlbumModel> deleteAlbum(int id) async {
  final response = await http
      .delete(Uri.parse('https://jsonplaceholder.typicode.com/albums/$id'));

  if (response.statusCode == HttpStatus.ok) {
    return AlbumModel.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to delete album.');
  }
}

Future<AlbumModel> updateAlbum(String title, int id) async {
  final response = await http.put(
      Uri.parse('https://jsonplaceholder.typicode.com/albums/$id'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'title': title,
      }));

  if (response.statusCode == HttpStatus.ok) {
    return AlbumModel.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to update album');
  }
}

class DetailScreen extends StatefulWidget {
  const DetailScreen({Key? key, required this.albumId}) : super(key: key);

  static const routeName = '/deleteScreen';

  final int albumId;

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  final TextEditingController _controller = TextEditingController();
  late Future<AlbumModel> _futureAlbum;

  @override
  void initState() {
    super.initState();
    _futureAlbum = fetchAlbum(widget.albumId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detail Screen'),
      ),
      body: Center(
        child: FutureBuilder<AlbumModel>(
          future: _futureAlbum,
          builder: (context, snapshot) {
            // If the connection is done,
            // check for response data or an error.
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasData) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(snapshot.data?.title ?? 'Deleted'),
                    ElevatedButton(
                      child: const Text('Delete Data'),
                      onPressed: () {
                        setState(() {
                          _futureAlbum = deleteAlbum(snapshot.data!.id!);
                        });
                      },
                    ),
                    Container(
                      height: 100,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        controller: _controller,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Enter text'),
                      ),
                    ),
                    ElevatedButton(
                      child: const Text('Update Data'),
                      onPressed: () {
                        setState(() {
                          _futureAlbum =
                              updateAlbum(_controller.text, widget.albumId);
                        });
                      },
                    ),
                  ],
                );
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              }
            }
            return const CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}
